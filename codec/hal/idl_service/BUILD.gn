# Copyright (c) 2022-2023 Shenzhen Kaihong DID Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import("//build/ohos.gni")
import("../..//codec.gni")

ohos_shared_library("libcodec_component_manager_service_2.0") {
  include_dirs = [
    "../../interfaces/include/",
    "../../utils/include/",
    "//third_party/openmax/api/1.1.2",
    "include",
  ]
  sources = [
    "../../utils/src/codec_hcb_util.c",
    "src/codec_component_config.cpp",
    "src/codec_component_manager_service.cpp",
    "src/codec_component_service.cpp",
    "src/codec_death_recipient.cpp",
    "src/codec_dfx_service.cpp",
    "src/codec_dma_buffer.cpp",
    "src/codec_dyna_buffer.cpp",
    "src/codec_handle_buffer.cpp",
    "src/codec_omx_core.cpp",
    "src/codec_share_buffer.cpp",
    "src/component_mgr.cpp",
    "src/component_node.cpp",
    "src/icodec_buffer.cpp",
  ]

  if (is_standard_system) {
    external_deps = [
      "c_utils:utils",
      "drivers_interface_codec:codec_idl_headers",
      "graphic_surface:buffer_handle",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hilog:libhilog",
      "hitrace:hitrace_meter",
      "ipc:ipc_single",
    ]
  } else {
    external_deps = [
      "hilog:libhilog",
      "ipc:ipc_single",
    ]
  }
  defines = []
  if (drivers_peripheral_codec_feature_set_omx_role) {
    defines += [ "SUPPORT_ROLE" ]
  }
  if (use_musl && use_jemalloc && use_jemalloc_dfx_intf) {
    defines += [ "CONFIG_USE_JEMALLOC_DFX_INTF" ]
  }
  license_file = "//third_party/openmax/README.OpenSource"
  innerapi_tags = [ "passthrough_indirect" ]
  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_codec"
}

ohos_shared_library("libcodec_driver") {
  include_dirs = [
    "../../interfaces/include/",
    "../../utils/include/",
    "//third_party/openmax/api/1.1.2",
    "include",
  ]

  sources = [ "src/codec_component_manager_driver.cpp" ]

  deps = [ ":libcodec_component_manager_service_2.0" ]

  if (is_standard_system) {
    external_deps = [
      "c_utils:utils",
      "drivers_interface_codec:codec_idl_headers",
      "drivers_interface_codec:libcodec_stub_2.0",
      "graphic_surface:buffer_handle",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hilog:libhilog",
      "ipc:ipc_single",
    ]
  } else {
    external_deps = [
      "hilog:libhilog",
      "ipc:ipc_single",
    ]
  }
  license_file = "//third_party/openmax/README.OpenSource"
  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_codec"
  shlib_type = "hdi"
}

group("codec_idl_omx_service") {
  deps = [
    ":libcodec_component_manager_service_2.0",
    ":libcodec_driver",
  ]
}
